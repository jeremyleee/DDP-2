public class Manusia {
    /*Instansi variabel*/

    private String nama;
    private int umur;
    private int uang;
    private double kebahagiaan=50;

    /*membuat constructor*/
    public Manusia(String nama,int umur,int uang){
        this.nama=nama;
        this.umur=umur;
        this.uang=uang;
    }
    public Manusia(String nama,int umur){
        this(nama,umur,50000);
    }
    /*membuat getter dan setter*/
    public String getNama() {
        return nama;
    }

    public int getUmur() {
        return umur;
    }

    public int getUang() {
        return uang;
    }

    public double getKebahagiaan()

    {
        return kebahagiaan;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public void setUang(int uang) {
        this.uang = uang;
    }

    public void setKebahagiaan(double kebahagiaan) {
        this.kebahagiaan = kebahagiaan;
    }
    /*membuat method-method*/

    /*membuat method beriUang*/
    public void beriUang(Manusia penerima) {
        int panjang = penerima.nama.length();
        int uangdiberi = 0;
        for (int a = 0; a < panjang; a++) {
            char huruf = penerima.nama.charAt(a);
            int ascii = (int) huruf;
            uangdiberi = uangdiberi + ascii;
        }
        uangdiberi=uangdiberi*100;
        if (uangdiberi > this.uang) {
            System.out.println(this.nama + "ingin memmberi uang kepada " + penerima.nama + " namun tidak memiliki cukup uang :'(");

        } else {
            this.uang = this.uang - (uangdiberi);
            penerima.uang = penerima.uang + (uangdiberi);
            this.kebahagiaan = this.kebahagiaan +((double)uangdiberi/ 6000);
            this.kebahagiaan = Math.min(100, this.kebahagiaan);
            penerima.kebahagiaan = penerima.kebahagiaan +((double)uangdiberi/ 6000);
            penerima.kebahagiaan = Math.min(100, penerima.kebahagiaan);
            System.out.println(this.nama + " memberi uang sebanyak " + uangdiberi + " kepada " + penerima.nama + " mereka berdua senang :D");

        }
    }
    /*membuat method beriUang jika diberi jumlah uang yang ingin diberi*/
        public void beriUang(Manusia penerima,int jumlahuang){
            if (jumlahuang>this.uang){
                System.out.println(this.nama + " ingin memmberi uang kepada " + penerima.nama + " namun tidak memiliki cukup uang :'(");
            }
            else {
                this.uang = this.uang - jumlahuang;
                penerima.uang = penerima.uang + jumlahuang;
                this.kebahagiaan = this.kebahagiaan + ((double)jumlahuang / 6000);
                this.kebahagiaan = Math.min(100, this.kebahagiaan);
                penerima.kebahagiaan = penerima.kebahagiaan + ((double)jumlahuang / 6000);
                penerima.kebahagiaan = Math.min(100, penerima.kebahagiaan);
                System.out.println(this.nama +" memberi uang sebanyak " + jumlahuang + " kepada " + penerima.nama + " mereka berdua senang :D");
            }
        }
        /*membuat method bekerja*/
        public void bekerja(int durasi,int bebanKerja){
        if (this.umur<18){
            System.out.println(this.nama+" belum boleh bekerja karena masih dibawah umur D:");
        }
        else{
            int BebanKerjaTotal;
            int Pendapatan;
            BebanKerjaTotal=durasi*bebanKerja;
            if (BebanKerjaTotal<=this.kebahagiaan) {
                this.kebahagiaan = this.kebahagiaan - BebanKerjaTotal;
                Pendapatan = BebanKerjaTotal * 10000;
                System.out.println(this.nama+" bekerja full time, total pendapatan :"+Pendapatan);
                this.uang=this.uang+Pendapatan;
            }
            else{
                double DurasiBaru;
                DurasiBaru=this.kebahagiaan/bebanKerja;
                int DurasiBaru1=(int) DurasiBaru;
                    BebanKerjaTotal = DurasiBaru1 * bebanKerja;
                    Pendapatan = BebanKerjaTotal * 10000;
                    this.kebahagiaan = this.kebahagiaan - BebanKerjaTotal;
                this.uang=this.uang+Pendapatan;
                  System.out.println(this.nama+" tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan :"+Pendapatan );
                }

            }
        }
        /*membuat method rekreasi*/
        public void rekreasi(String namaTempat) {
            int Biaya;
            int panjangnama = namaTempat.length();
            Biaya = panjangnama * 10000;
            if (Biaya > this.uang) {
                System.out.println(this.nama + "tidak bisa pergi ke " + namaTempat + " karena kurangnya biaya...");
            } else {
                this.kebahagiaan = this.kebahagiaan + panjangnama;
                this.kebahagiaan = Math.min(100, this.kebahagiaan);
                this.uang = this.uang - Biaya;
                System.out.println(this.nama + " berekreasi ke " + namaTempat + "," + this.nama + " senang!");
            }
        }
        /*membuat method sakit*/
         public void sakit(String namaPenyakit){
                int penyakit=namaPenyakit.length();
                this.kebahagiaan=this.kebahagiaan-penyakit;
                this.kebahagiaan = Math.max(0, this.kebahagiaan);
                System.out.println(this.nama+" terkena penyakit "+namaPenyakit+",Segera periksakan ke dokter!!!");
            }
            /*membuat method toString*/
            public String toString(){
            return ("Nama\t\t:"+this.nama+
                "\nUmur\t\t:"+this.umur+
                "\nUang\t\t:"+this.uang+
                "\nKebahagiaan\t:"+this.kebahagiaan);
            }
        }
