import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author ....., NPM ....., Kelas ....., GitLab Account: .....
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama =input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		String panjang = input.nextLine();
		int panjang1 =Integer.parseInt(panjang);
		System.out.print("Lebar Tubuh (cm)       : ");
		String lebar =input.nextLine();
		int lebar1 =Integer.parseInt(lebar);
		System.out.print("Tinggi Tubuh (cm)      : ");
		String tinggi = input.nextLine();
		int tinggi1 =Integer.parseInt(tinggi);
		System.out.print("Berat Tubuh (kg)       : ");
		String berat = input.nextLine();
		float berat1 =Integer.parseInt(berat);
		System.out.print("Jumlah Anggota Keluarga: ");
		String makanan = input.nextLine();
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		String jumlahCetakan = input.nextLine();
		int jumlahCetakan1 =Integer.parseInt(jumlahCetakan);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		float rasio = berat1/((panjang1/100)*(lebar1/100)*(tinggi1/100));
		int rasio1= (int) rasio;
		for (int a=1;a<=jumlahCetakan1;a++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " +a+ " dari " + jumlahCetakan1 + " untuk: ");
			String penerima =input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase
			System.out.print("DATA SIAP DICETAK UNTUK " + penerima + "\n--------------------\n" );
			System.out.println(nama + " - " + alamat);
			System.out.println("Lahir pada tanggal " + tanggalLahir);
			System.out.println("Rasio Berat per Volume     = " + rasio1 + " kg/m^3");
			// TODO Periksa ada catatan atau tidak
			if (catatan=="") {catatan = "Tidak ada catatan tambahan";}
			else {catatan =catatan;}
			System.out.println("Catatan :"+catatan);
		}


		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)



		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		//String nomorKeluarga = "";

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		//... anggaran = (...) (...);

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		//... tahunLahir = .....; // lihat hint jika bingung
		//... umur = (...) (...);

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)





		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		//String rekomendasi = "";
		//.....;

		input.close();
	}
}